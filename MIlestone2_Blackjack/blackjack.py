'''
Blackjack.py
Date Modified: 11/27/2019
Author: Craig Uss

This is the main class
'''
from model.gameplay import Gameplay

player_name = input('What is your name -->')
gameplay = Gameplay(player_name)

while gameplay.player.bank > 0:
    gameplay.start_game()
    gameplay.generate_board()
    bet = gameplay.ask_for_bet()

    while True:
        result = gameplay.hit_or_stay()
        gameplay.generate_board()

        if result == 1: #player wins
            print('YOU WON')
            winnings = int(bet)*2
            gameplay.player_win(winnings)
            break
        elif result == 2: #dealer wins
            print('Sorry, dealer one')
            winnings = int(bet)*(-1)
            gameplay.player_win(winnings)
            break
        elif result == 3: #Draw
            print('It\'s a draw!')
            break
        elif result ==4: #no winner yet
            continue

    while True:
        next_game = input('Do you want to play a new game? Y or N -->')

        if next_game == 'Y':
            break
        elif next_game == 'N':
            quit()

        continue

if gameplay.player.bank == 0:
    print('You are out of money! Game Over!')