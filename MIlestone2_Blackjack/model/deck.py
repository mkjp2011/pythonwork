'''
Deck.py
created: 11/26/2019
author: Craig Uss
'''
from random import seed
from random import shuffle
from model.card import Card

class Deck:
    '''
    A deck is comprised of 55 Card objects
    '''
    MaxNoOfCards = 56
    deckofcards = []
    activecards = []

    def __init__(self):
        '''
        Need to initialize the deck with 52 cards
        will Call the Card objects acceptable values to build out the deck
        '''
        self.reset_deck()

    def __str__(self):
        '''
        will print out deck of cards
        '''
        for card in self.deckofcards:
            print(card.printcard())

    def draw(self):
        '''
        Draw card from deckoffcards and add it to the activecards list
        '''
        while True:
            if len(self.deckofcards) >= 1:
                newcard = self.deckofcards.pop()
                self.activecards.append(newcard)
                return newcard
            #Need to reshuffle
            print('Reshuffling the deck')
            shuffle(self.deckofcards)
            #Delete active cards except for the ones in players hands
            del self.activecards[3:]
            for card in self.activecards:
                self.deckofcards.remove(card)
            continue

    def shuffle(self):
        '''
        shuffle the deck
        '''
        #seed(self.MaxNoOfCards)
        shuffle(self.deckofcards)

    def reset_deck(self):
        '''
        Resets the deck of cards
        '''
        for suit in Card.Suits:
            for cardvalue in Card.Values:
                card = Card(suit, cardvalue)
                self.deckofcards.append(card)

        self.shuffle()

    def deckcardcount(self):
        '''
        to get the number of cards in deck currrently
        '''
        return len(self.deckofcards)
