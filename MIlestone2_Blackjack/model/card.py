'''
Card.py
created: 11/26/2019
author: Craig Uss
'''
class Card:
    '''
    Will represent a card within a deck
    '''

    Suits = ['heart', 'spade', 'diamond', 'club']
    Values = ['Ace', 'King', 'Queen', 'Jack',
              '10', '9', '8', '7', '6', '5', '4', '3', '2', '1']

    #Setting up card values (Graphical)
    Heart_card = r"""
     ______
    |AA_ _ |
    | ( v )|
    |  \ / |
    |   .  |
    |____AA|"""

    Spade_card = r"""
     ______
    |AA .  |
    |  /.\ |
    | (_._)|
    |   |  |
    |____AA|"""

    Club_card = r"""
     ______
    |AA _  |
    |  ( ) |
    | (_'_)|
    |   |  |
    |____AA|"""
    Diamond_card = r"""
     ______
    |AA ^  |
    |  / \ |
    |  \ / |
    |   .  |
    |____AA|"""

    Hidden_card = r"""
     ______
    |\ ~~ /|
    |}}::{{|
    |}}::{{|
    |}}::{{|
    |/_~~_\|"""

    def __init__(self, suit, value, hidden=False):
        '''
        Initialize the card
        '''
        if suit in self.Suits:
            self.suit = suit
        else:
            raise TypeError('Not an acceptable suit.')

        if value in self.Values:
            self.value = value
        else:
            raise TypeError('Value of card is not acceptable')

        self.hidden = hidden

    def __str__(self):
        '''
        print out the card
        '''
        return "%s of %s" %(self.value, self.suit)

    def valueof(self):
        '''
        Print out the card
        '''
        return "%s of %s" %(self.value, self.suit)

    def printcard(self):
        '''
        printing the card
        '''
        cardstring = ''
        if not self.hidden:
            if self.value in ['Ace', 'King', 'Queen', 'Jack']:
                value_card = self.value[0].upper().ljust(2, ' ')
            else:
                value_card = self.value.ljust(2, ' ')
            if self.suit == self.Suits[0]:
                cardstring = self.Heart_card.replace('AA', value_card)
            elif self.suit == self.Suits[1]:
                cardstring = self.Spade_card.replace('AA', value_card)
            elif self.suit == self.Suits[2]:
                cardstring = self.Diamond_card.replace('AA', value_card)
            elif self.suit == self.Suits[3]:
                cardstring = self.Club_card.replace('AA', value_card)
        else:
            cardstring = self.Hidden_card
        return cardstring

    def getacceptedsuits(self):
        '''
        allow objects to validate and build
        '''
        return self.Suits

    def getacceptedvalues(self):
        '''
        allow objects to validate and build
        '''
        return self.Values

    def ishidden(self, setting):
        '''
        will mark whether a card should be shown on screen or not
        '''
        self.hidden = setting
