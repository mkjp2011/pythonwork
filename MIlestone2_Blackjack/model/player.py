'''
Player.py
created: 11/26/2019
author: Craig Uss
'''

class Player:
    '''
    This class will represent a player within the game
    '''
    hand = []
    player_total = [0, 0]

    def __init__(self, name, bank=500):
        self.name = name
        self.bank = bank
        self.reset_hand()

    def update_bank(self, winnings):
        '''
        This can be used to update a players bank
        input can be positive or negative values
        '''
        self.bank += winnings

    def calculate_totals(self):
        '''
        this will recalculate a players total based on their current HAND
        '''
        new_hand = self.hand
        self.player_total = [0, 0]
        for card in new_hand:
            if card.value == 'Ace':
                self.player_total[0] += 10
                self.player_total[1] += 1
            elif card.value in ['King', 'Queen', 'Jack']:
                self.player_total[0] += 10
            else:
                self.player_total[0] += int(card.value)

    def add_to_hand(self, card):
        '''
        will add card to current players HAND
        '''
        self.hand.append(card)
        self.calculate_totals()

    def get_player_total(self):
        '''
        easy way to get the players total
        '''
        return self.player_total

    def reset_hand(self):
        '''
        Will reset players HAND
        '''
        self.hand = []

    def get_bank(self):
        '''
        return palyers bank
        '''
        return self.bank

    def get_player_name(self):
        '''
        return players name
        '''
        return self.name

    def get_player_hand(self):
        '''
        Need to return player hand for display
        '''
        return self.hand

    def set_card_hidden(self, setting):
        '''
        set a card to be hidden
        '''
        if setting:
            self.hand[1].ishidden(True)
        else:
            self.hand[1].ishidden(False)

    def __str__(self):
        '''
        return a representation of the player class
        '''
        return_string = '%s - total --> %s - Bank --> %s' 
        return return_string %(self.name, self.player_total, self.bank)
