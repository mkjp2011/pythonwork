'''
Gameplay.py
created: 11/26/2019
author: Craig Uss
'''
from model.deck import Deck
from model.player import Player

class Gameplay:
    '''
    This class will represent gameplay actions
    '''

    def __init__(self, playerName):
        self.dealer = Player('dealer')
        self.player = Player(playerName)
        self.deck = Deck()

    def start_game(self):
        '''
        This will restart the game
        '''
        self.deck.reset_deck()

        self.dealer.reset_hand()
        self.player.reset_hand()

        #Build hands for actors
        self.player.add_to_hand(self.deck.draw())
        self.dealer.add_to_hand(self.deck.draw())
        self.player.add_to_hand(self.deck.draw())
        self.dealer.add_to_hand(self.deck.draw())

        self.dealer.set_card_hidden(True)

        return self.check_for_win(False)

    def hit(self):
        '''
        Logic to take a HIT action
        '''
        self.player.add_to_hand(self.deck.draw())
        return self.check_for_win(False)

    def stay(self):
        '''
        Logic for STAY action
        '''
        self.dealer.set_card_hidden(False)
        self.dealer.add_to_hand(self.deck.draw())
        return self.check_for_win(True)

    def generate_board(self):
        '''
        Will use this to update board on screen
        '''
        divider = '-'
        print(divider*80)
        print('Dealer - ')
        for card in self.dealer.get_player_hand():
            print(card.printcard())
        print(divider*80)
        print(f'{self.player}')
        for card in self.player.get_player_hand():
            print(card.printcard())
        print(divider*80)

    @staticmethod
    def ask_for_bet():
        '''
        Prompt for bet
        '''
        while True:
            try:
                return int(input('Place your bets, please --> '))
            except TypeError:
                print('Please provide us a whole number only!')
                continue

    def hit_or_stay(self):
        '''
        Prompt for hit or stay
        '''
        while True:
            try:
                action = int(input('Would you like to (1) Hit or (2) stay? -->'))

                if action in [1, 2]:
                    break
                continue
            except TypeError:
                print('Please provide a 1 or 2 to pick an option')
                continue

        if action == 1:
           result = self.hit()
        elif action == 2:
           result = self.stay()

        return result

    def check_for_win(self, final_move):
        '''
        determine if there are any winners
        1 = player win
        2 = dealer win
        3 = no winner yet
        '''
        win_value = 3
        dealer_total = self.dealer.get_player_total()[0]
        player_total = self.player.get_player_total()[0]
        if dealer_total > 21:
            win_value = 1
        elif player_total > 21:
            win_value = 2
        elif dealer_total == 21:
            win_value = 2
        elif player_total == 21:
            win_value = 1
        elif player_total > dealer_total and player_total <= 21 and final_move:
            win_value = 1
        elif player_total < dealer_total and final_move:
            win_value = 2
        elif player_total == dealer_total and final_move:
            win_value = 3
        else:
            win_value = 4

        return win_value

    def player_win(self,amount):
        '''
        process winnings
        '''
        self.player.update_bank(amount)
        
