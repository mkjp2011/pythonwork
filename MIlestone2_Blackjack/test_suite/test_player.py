'''
testing the card implementation
'''
import unittest
from model.card import Card
from model.player import Player

class Test_Player(unittest.TestCase):

    def test_basic_player(self):

        #Create player
        name = 'Craig'
        player = Player(name)
        #print(three_of_hearts.printcard())
        self.assertEqual(player.get_bank(),500)

    def test_player_card_add(self):
        suit='heart'
        value='3'
        #create the card
        card = Card(suit,value)

        #Create player
        name = 'Craig'
        player = Player(name)
        player.add_to_hand(card)
        #print(three_of_hearts.printcard())
        self.assertEqual(player.get_player_total(),[3,0])

    def test_player_multiple_cards(self):
        suit='heart'
        value='5'
        #create the card
        card1 = Card(suit,value)

        suit='diamond'
        value='Ace'
        #create the card
        card2 = Card(suit,value)

        #Create player
        name = 'Craig'
        player = Player(name)
        player.add_to_hand(card1)
        player.add_to_hand(card2)
        #print(three_of_hearts.printcard())
        self.assertEqual(player.get_player_total(),[15,1])

    def test_player_bank(self):
        #Create player
        name = 'Craig'
        player = Player(name)
        player.update_bank(-100)
        player.update_bank(300)
        #print(three_of_hearts.printcard())
        self.assertEqual(player.get_bank(),700)

if __name__ == '__main__':
    unittest.main()