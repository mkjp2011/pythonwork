'''
testing the card implementation
'''
import unittest
from model.card import Card

class Test_Card(unittest.TestCase):

    def test_three_heart_card(self):
        suit='heart'
        value='3'
        #create the card
        three_of_hearts = Card(suit,value)
        #print(three_of_hearts.printcard())
        self.assertEqual(three_of_hearts.valueof(),'3 of heart')

    def test_10_heart_card(self):
        suit='heart'
        value='10'
        #create the card
        three_of_hearts = Card(suit,value)
        #print(three_of_hearts.printcard())
        self.assertEqual(three_of_hearts.valueof(),'10 of heart')

    def test_Ace_heart_card(self):
        suit='heart'
        value='Ace'
        #create the card
        three_of_hearts = Card(suit,value)
        #print(three_of_hearts.printcard())
        self.assertEqual(three_of_hearts.valueof(),'Ace of heart')

    def test_invalid_value_card(self):
        suit='heart'
        value='11'
        try:
            card = Card(suit,value)
        except TypeError:
            pass
        else:
            self.assertTrue(False)

    def test_invalid_suit_card(self):
        suit='diheart'
        value='10'
        try:
            card = Card(suit,value)
        except TypeError:
            pass
        else:
            self.assertTrue(False)
            
    def test_King_spade_card(self):
        suit='spade'
        value='King'
        #create the card
        three_of_hearts = Card(suit,value)
        #print(three_of_hearts.printcard())
        self.assertEqual(three_of_hearts.valueof(),'King of spade')

if __name__ == '__main__':
    unittest.main()