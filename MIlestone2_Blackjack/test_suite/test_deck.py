'''
testing the deck implementation
'''
import unittest
from model.deck import Deck
from model.card import Card

class Test_Card(unittest.TestCase):

    def test_basic_deck(self):
        myDeck = Deck()
        myCard = myDeck.draw()
        #print(myCard.printcard())
        #print(myDeck.deckcardcount())
        #print(myDeck.MaxNoOfCards)
        self.assertTrue(myDeck.deckcardcount() < myDeck.MaxNoOfCards)

if __name__ == '__main__':
    unittest.main()